#!/bin/bash
#Auteur : Saada Antoine 1a4b

#Présentation du Script 
echo " 

 ______   _______  ______           _________
(  __  \ (  ____ \(  ___ \ |\     /|\__   __/
| (  \  )| (    \/| (   ) )| )   ( |   ) (   
| |   ) || (__    | (__/ / | |   | |   | |   
| |   | ||  __)   |  __ (  | |   | |   | |   
| |   ) || (      | (  \ \ | |   | |   | |   
| (__/  )| (____/\| )___) )| (___) |   | |   
(______/ (_______/|/ \___/ (_______)   )_(                                          
"
echo "  "
echo "Ce programme vous permettra après lancement d'installer tout ce dont vous aurez besoins sur votre nouvelle machine pour travailler convenablement"
echo "  "
echo " Durant tout le long du processus de téléchargement il vous sera posé des questions auxquelles vous pourrez répondre O pour oui ou N pour non"
echo "  "
echo "Bonne installation ! :)"

#Installation de Visual Studio Code et Extensions
echo "  "
echo "Souhaitez-vous télécharger Visual Studio Code sur cette machine ? [O/N]"
read rep 
if [ $rep = "O" ] || [ $rep = "o" ]
then 
  sudo apt update -y
  sudo apt install software-properties-common apt-transport-https wget -y
  wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
  sudo add-apt-repository "deb https://packages.microsoft.com/repos/vscode stable main"
  sudo apt update -y 
  sudo apt install code -y
  code --install-extension ms-python.python
  code --install-extension njpwerner.autodocstring
  code --install-extension gitlab.gitlab-workflow
  code --install-extension ms-python.vscode-pylance
  echo "   "
  echo "   "
  echo "Visual Studio Code s'est téléchargé et installé avec succès, les extensions Python, Autodocstring, Gitlab ainsi que Pylance ont aussi été installé"
fi

#Installation de Java
echo "  "
echo "Souhaitez-vous installer Java sur cette machine ? [O/N]"
read rep
if [ $rep = "O" ] || [ $rep = "o" ]
then
  sudo apt-get update -y
  sudo apt-get install default-jre -y 
  sudo apt install default-jdk -y
  echo "Souhaitez-vous lancer le fichier Executable.java [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    touch Executable.java
    cat <<EOF Executable.java
public
class Executable{
  public
static void main(String[] args){

	  System.out.println(" Hello
World !") ;
  }
}
EOF
  javac Executable.java
  echo "   "
  echo "   "
  echo "Le programme s'est executé avec succès"
  fi
fi

#Installation de Python 3.10
echo " "
echo "Souhaitez-vous télécharger Python 3.10 ? [O/N] "
read rep
if [ $rep = "O" ] || [ $rep = "o" ]
then
  sudo apt-get update -y
  sudo apt-get install python3.10
  echo " Voulez vous essayer de lancer le programme python ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    cat <<EOF | python3 -
import sys;
print("Hello World !")
EOF
    echo " "
    echo "Le programme s'est executé avec succès"
  fi
fi

#Installation de Docker 
echo " "
echo "Souhaitez-vous télécharger Docker ? [O/N]"
read rep 
if [ $rep = "O" ] || [ $rep = "o" ]
then
  sudo apt update
  sudo apt install apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
  sudo apt update
  apt-cache policy docker-ce
  sudo apt install docker-ce
  sudo apt install docker
  echo "Voulez-vous lancer le Docker hello-world afin d'essayer l'installation [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then 
    sudo docker run hello-world
  fi
fi

#Création des dossiers
echo "Souhaitez-vous créer différents dossiers vous permettants de trier vos cours ? [O/N]"
read rep
if [ $rep = "O" ] || [ $rep = "o" ]
then
  cd ~/Documents
  mkdir "BUT Informatique 1ère Année"
  cd BUT\ Informatique\ 1ère\ Année  
  echo "Voulez vous un dossier pour ranger vos cours de Bases de données ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Bases de données"
  fi
  echo "Voulez vous un dossier pour ranger vos SAE ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "SAE"
  fi
  echo "Voulez vous un dossier pour ranger vos cours d'Initiation au Dev ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Initiation au Dev"
  fi
  echo "Voulez vous un dossier pour ranger vos cours de Mathématiques ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Mathématiques"
  fi
  echo "Voulez vous un dossier pour ranger vos cours de Gestion de projet ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Gestion de projet"
  fi
  echo "Voulez vous un dossier pour ranger vos cours d'Anglais ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Anglais"
  fi
  echo "Voulez vous un dossier pour ranger vos cours de Bash ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Bash"
  fi
  echo "Voulez vous un dossier pour ranger vos cours de Développement Web ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Développement Web"
  fi
  echo "Voulez vous un dossier pour ranger vos cours d'Economie ? [O/N]"
  read rep
  if [ $rep = "O" ] || [ $rep = "o" ]
  then
    mkdir "Economie"
  fi
  echo "  "
  echo "  "
  echo "Les dossiers demandé ont été crée avec succès"
fi
echo "
 _______ _________ _       
(  ____ \\__   __/( (    /|
| (    \/   ) (   |  \  ( |
| (__       | |   |   \ | |
|  __)      | |   | (\ \) |
| (         | |   | | \   |
| )      ___) (___| )  \  |
|/       \_______/|/    )_)
                           
"

