Pour commencer, j'utilise Linux sur la dernière version de Ubuntu en dual boot sur mon ordinateur portable.
Pour faire des essaie de mon code j'utilisais également une machine virtuel aussi sous ubuntu dernière version afin de réaliser des test.
Pour reproduire mon installation il suffit de lancer le fichier SAE_Bash_Installation.sh qui est dans ce même dossier (en lui donnant les droits)
Il faudra surement également rentrer votre mot de passe pour les commandes sudo.
J'ai séparé mon script en plusieurs parties:
						1-Installation de Visual Studio Code et Extensions
						2-Installation de Java et lancement de la fonction test
						3-Installation de Python 3.10 et lancement de la fonction test
						4-Installation de Docker et lancement de la fonction test 
						5-Création de différents dossiers pour ranger ses cours
Pour réaliser cette installation, après avoir lancé le programme depuis le bon dossier avec la commande ''bash SAE_Bash_Installation.sh,
il vous faudra répondre aux différentes questions qui seront affiché sur votre terminale avec O pour oui et N pour non (peut importe majuscule ou minuscule).
Si vous avez déjà sur votre pc un des éléments qu'il vous demande d'installer vous pouvez donc mettre non pour passer directement a la suite et éviter tout message d'erreur.
  
1-Installation de Visual Studio Code :
	Je commence par demander si l'utilisateur souhaite installer ce programme puis je lis sa réponse
	Si il a dit oui, je mets à jour la liste des fichiers disponible
	J'installe les dépendances et j'importe la clé Microsoft GPG avec la commande wget 
	Ensuite j'active le référentiel de VSCODE 
	Pour finir j'installe la dernière version de VSCODE

Extensions :
	J'installe 4 extensions avec la commande code --install-extension qui sont python, pylint, autodocstring et gitlab
	Pour finir cette première partie, j'envoie un message indiquant que l'installation s'est bien passée 

2-Installation de Java et execution de la fonction
	Je commence par demander si l'utilisateur souhaite installer ce programme puis je lis sa réponse
	Si il a dit oui, je mets à jour la liste des fichiers disponible
	Pour finir j'installe la dernière version de Java
	Je demande si l'utilisateur souhaite lancer le programme Executable.java

3-Installation de Python 3.10 et execution de la fonction
	Je commence par demander si l'utilisateur souhaite installer ce programme puis je lis sa réponse
	Si il a dit oui, je mets à jour la liste des fichiers disponible
	Pour finir j'installe la dernière version de Python
	Je demande si l'utilisateur souhaite lancer la fonction print('Hello world')

4-Installation de docker et execution de la fonction
	Je commence par demander si l'utilisateur souhaite installer ce programme puis je lis sa réponse
	Si il a dit oui, je mets à jour la liste des fichiers disponible
	J'ajoute la clé GPG de Docker et j'autorise l'apt utilisation d'un referentiel https
	Pour finir j'installe la dernière version de docker et je demande si l'utilisateur souhaite executer docker hello-world afin de vérifier que docker est bien installé 

5-Création des dossiers
	Je demande a l'utilisateur si il a besoin de créer un dossier pour les cours 
	Si il répond oui, pour chaque matière je demande a l'utilisateur si il veut créer un dossier correspondant


	

	 